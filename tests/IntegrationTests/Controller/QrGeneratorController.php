<?php

namespace App\Tests\IntegrationTests\Controller;

use App\Tests\UnitTests\Utils\FakerProviderTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class QrGeneratorController extends WebTestCase
{
    use FakerProviderTrait;

    private KernelBrowser $client;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();
    }

    /**
     * @return void
     */
    public function testIndexAction(): void
    {
        $this->client->request('GET', '/');
        self::assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}