<?php

namespace App\Tests\UnitTests\Service;

use App\Form\Model\QrCode;
use App\Service\ApiClient;
use App\Tests\UnitTests\Utils\FakerProviderTrait;
use DateTime;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionException;
use RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\RedirectionException;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ApiClientTest extends TestCase
{
    use FakerProviderTrait;

    private MockObject $client;
    private ParameterBagInterface $parameterBag;
    private ApiClient $apiClient;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $faker = $this->getFaker();

        /** @var HttpClientInterface|MockObject $client */
        $client = $this->createMock(HttpClientInterface::class);
        $this->client = $client;

        $parameterBag = new ParameterBag();
        $this->parameterBag = $parameterBag;

        $this->parameterBag->set('apiUser', $faker->userName);
        $this->parameterBag->set('apiPassword', $faker->password);

        $this->apiClient = new ApiClient($this->client, $this->parameterBag);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->apiClient);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testMakeRequestTransportException(): void
    {
        $faker = $this->getFaker();
        $this->expectException(RuntimeException::class);

        $this->client->method('request')->withAnyParameters()->willThrowException(new TransportException());
        $this->invokeMethod($this->apiClient, 'makeRequest', [
            'url' => $faker->url,
            'headers' => [],
            'body' => $faker->uuid,
        ]);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testMakeRequestClientException(): void
    {
        $faker = $this->getFaker();
        $this->expectException(RuntimeException::class);

        $this->client->method('request')->withAnyParameters()->willThrowException(
            new ClientException(new MockResponse()),
        );
        $this->invokeMethod($this->apiClient, 'makeRequest', [
            'url' => $faker->url,
            'headers' => [],
            'body' => $faker->uuid,
        ]);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testMakeRequestRedirectException(): void
    {
        $faker = $this->getFaker();
        $this->expectException(RuntimeException::class);

        $this->client->method('request')->withAnyParameters()->willThrowException(
            new RedirectionException(new MockResponse()),
        );
        $this->invokeMethod($this->apiClient, 'makeRequest', [
            'url' => $faker->url,
            'headers' => [],
            'body' => $faker->uuid,
        ]);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testMakeRequestDecodingException(): void
    {
        $faker = $this->getFaker();
        $exception = $this->createMock(DecodingExceptionInterface::class);
        $this->expectException(RuntimeException::class);

        $this->client->method('request')->withAnyParameters()->willThrowException($exception);
        $this->invokeMethod($this->apiClient, 'makeRequest', [
            'url' => $faker->url,
            'headers' => [],
            'body' => $faker->uuid,
        ]);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testMakeRequestServerException(): void
    {
        $faker = $this->getFaker();
        $this->expectException(RuntimeException::class);

        $this->client->method('request')->withAnyParameters()->willThrowException(
            new ServerException(new MockResponse()),
        );
        $this->invokeMethod($this->apiClient, 'makeRequest', [
            'url' => $faker->url,
            'headers' => [],
            'body' => $faker->uuid,
        ]);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGenerateQrCode(): void
    {
        $faker = $this->getFaker();
        $encodedImage = $faker->uuid;
        $data = $this->getDataForQrCode();

        $qrCode = new QrCode();
        $qrCode
            ->setIban($data['iban'])
            ->setSpecificSymbol($data['specificSymbol'])
            ->setConstantSymbol($data['constantSymbol'])
            ->setVariableSymbol($data['variableSymbol'])
            ->setMessage($data['message'])
            ->setDueDate($data['dueDate'])
            ->setMoneyAmount($data['moneyAmount'])
            ->setMoneyCurrency($data['moneyCurrency'])
            ->setQrCodeScale($data['qrCodeScale'])
            ->setQrCodeMargin($data['qrCodeMargin'])
        ;

        $response = $this->createMock(ResponseInterface::class);
        $response->method('toArray')->willReturn([
            'data' => [
                'token' => $faker->uuid,
                'base64Data' => $encodedImage,
            ],
        ]);
        $this->client->method('request')->withAnyParameters()->willReturn($response);

        $result = $this->invokeMethod($this->apiClient, 'generateQrCode', [
            'data' => $qrCode->getQrCodeSettings(),
        ]);

        self::assertSame($encodedImage, $result);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGenerateQrCodeError(): void
    {
        $faker = $this->getFaker();
        $errorMessage = 'Error message';
        $data = [];

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage($errorMessage);

        $response = $this->createMock(ResponseInterface::class);
        $response->method('toArray')->willReturn([
            'data' => [
                'token' => $faker->uuid,
            ],
            'errors' => [['message' => $errorMessage]],
        ]);
        $this->client->method('request')->withAnyParameters()->willReturn($response);
        $this->invokeMethod($this->apiClient, 'generateQrCode', [
            'data' => $data,
        ]);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGenerateQrCodeEmpty(): void
    {
        $faker = $this->getFaker();
        $data = [];

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('QR code can not be empty');

        $response = $this->createMock(ResponseInterface::class);
        $response->method('toArray')->willReturn([
            'data' => [
                'token' => $faker->uuid,
            ],
        ]);
        $this->client->method('request')->withAnyParameters()->willReturn($response);
        $this->invokeMethod($this->apiClient, 'generateQrCode', [
            'data' => $data,
        ]);
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetApiToken(): void
    {
        self::assertEmpty($this->getProperty($this->apiClient, 'token'));
        self::assertNull($this->getProperty($this->apiClient, 'tokenExpiration'));

        $faker = $this->getFaker();
        $response = $this->createMock(ResponseInterface::class);
        $response->method('toArray')->willReturn(['data' => ['token' => $faker->uuid]]);

        $this->client->method('request')->withAnyParameters()->willReturn($response);
        $this->invokeMethod($this->apiClient, 'getApiToken');

        self::assertNotEmpty($this->getProperty($this->apiClient, 'token'));
        self::assertInstanceOf(DateTime::class, $this->getProperty($this->apiClient, 'tokenExpiration'));
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetUsername(): void
    {
        self::assertNotEmpty($this->invokeMethod($this->apiClient, 'getUsername'));
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetUsernameEmpty(): void
    {
        $this->parameterBag->set('apiUser', '');

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Username can not be empty');
        $this->invokeMethod($this->apiClient, 'getUsername');
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetPassword(): void
    {
        self::assertNotEmpty($this->invokeMethod($this->apiClient, 'getPassword'));
    }

    /**
     * @return void
     *
     * @throws ReflectionException
     */
    public function testGetPasswordEmpty(): void
    {
        $this->parameterBag->set('apiPassword', '');

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Password can not be empty');
        $this->invokeMethod($this->apiClient, 'getPassword');
    }

    /**
     * @param object                  $object
     * @param string                  $methodName
     * @param array<int, string|array $parameters
     *
     * @return mixed|string|null
     *
     * @throws ReflectionException
     */
    private function invokeMethod(object $object, string $methodName, array $parameters = [])
    {
        $reflection = new ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * @param object $object
     * @param string $propertyName
     *
     * @return string|DateTime|null
     *
     * @throws ReflectionException
     */
    private function getProperty(object $object, string $propertyName)
    {
        $reflection = new ReflectionClass(get_class($object));
        $property = $reflection->getProperty($propertyName);
        $property->setAccessible(true);

        return $property->getValue($object);
    }

    /**
     * @return array{
     *     iban: string, specificSymbol: int, constantSymbol: int, variableSymbol: int, message: string,
     *     dueDate: DateTime, moneyAmount: int, moneyCurrency: string, qrCodeScale: int, qrCodeMargin: int
     * }
     */
    private function getDataForQrCode(): array
    {
        return [
            'iban' => $this->faker->iban(),
            'specificSymbol' => $this->faker->randomNumber(),
            'constantSymbol' => $this->faker->randomNumber(),
            'variableSymbol' => $this->faker->randomNumber(),
            'message' => $this->faker->text(),
            'dueDate' => $this->faker->dateTime(),
            'moneyAmount' => $this->faker->randomNumber(),
            'moneyCurrency' => $this->faker->currencyCode,
            'qrCodeScale' => $this->faker->randomNumber(),
            'qrCodeMargin' => $this->faker->randomNumber(),
        ];
    }
}
