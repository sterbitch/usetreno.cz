<?php

namespace App\Tests\UnitTests\Utils;

use Faker\Factory;
use Faker\Generator;

trait FakerProviderTrait
{
    protected ?Generator $faker = null;

    /**
     * @param string $locale
     *
     * @return Generator
     */
    public function getFaker(string $locale = 'cs_CZ'): Generator
    {
        if (null === $this->faker) {
            $this->faker = Factory::create($locale);
        }

        return $this->faker;
    }
}
