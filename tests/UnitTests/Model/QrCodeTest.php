<?php

namespace App\Tests\UnitTests\Model;

use App\Form\Model\QrCode;
use App\Tests\UnitTests\Utils\FakerProviderTrait;
use DateTime;
use PHPUnit\Framework\TestCase;

class QrCodeTest extends TestCase
{
    use FakerProviderTrait;

    /**
     * Metoda spuštěna před každým testem
     */
    protected function setUp(): void
    {
        $this->object = new QrCode();
    }

    /**
     * Metoda spuštěna po každém testu
     */
    protected function tearDown(): void
    {
        unset($this->object);
    }

    /**
     * Otestuje počáteční getter pro iban
     */
    public function testInitialGetterIban(): void
    {
        $this->expectExceptionMessage(sprintf(
            'Typed property %s::$iban must not be accessed before initialization',
            QrCode::class,
        ));
        $this->object->getIban();
    }

    /**
     * Otestuje počáteční getter pro moneyAmount
     */
    public function testInitialGetterMoneyAmount(): void
    {
        $this->expectExceptionMessage(sprintf(
            'Typed property %s::$moneyAmount must not be accessed before initialization',
            QrCode::class,
        ));
        $this->object->getMoneyAmount();
    }

    /**
     * Otestuje počáteční getter pro moneyCurrency
     */
    public function testInitialGetterMoneyCurrency(): void
    {
        $this->expectExceptionMessage(sprintf(
            'Typed property %s::$moneyCurrency must not be accessed before initialization',
            QrCode::class,
        ));
        $this->object->getMoneyCurrency();
    }

    /**
     * Otestuje počáteční getter pro dueDate
     */
    public function testInitialGetterDueDate(): void
    {
        $this->expectExceptionMessage(sprintf(
            'Typed property %s::$dueDate must not be accessed before initialization',
            QrCode::class,
        ));
        $this->object->getDueDate();
    }

    /**
     * Otestuje počáteční getters
     */
    public function testInitialGetter(): void
    {
        self::assertNull($this->object->getVariableSymbol());
        self::assertNull($this->object->getConstantSymbol());
        self::assertNull($this->object->getSpecificSymbol());
        self::assertNull($this->object->getMessage());
        self::assertEquals(5, $this->object->getQrCodeScale());
        self::assertEquals(20, $this->object->getQrCodeMargin());
    }

    /**
     * Otestuje počáteční getters
     */
    public function testGetQrCodeSettings(): void
    {
        $data = $this->getDataForObject();

        $this->object
            ->setIban($data['iban'])
            ->setSpecificSymbol($data['specificSymbol'])
            ->setConstantSymbol($data['constantSymbol'])
            ->setVariableSymbol($data['variableSymbol'])
            ->setMessage($data['message'])
            ->setDueDate($data['dueDate'])
            ->setMoneyAmount($data['moneyAmount'])
            ->setMoneyCurrency($data['moneyCurrency'])
            ->setQrCodeScale($data['qrCodeScale'])
            ->setQrCodeMargin($data['qrCodeMargin'])
        ;

        $result = $this->object->getQrCodeSettings();

        self::assertSame($result['iban'], $data['iban']);
        self::assertSame($result['paymentIdentification']['specificSymbol'], (string) $data['specificSymbol']);
        self::assertSame($result['paymentIdentification']['constantSymbol'], (string) $data['constantSymbol']);
        self::assertSame($result['paymentIdentification']['variableSymbol'], (string) $data['variableSymbol']);
        self::assertSame($result['money']['amount'], (string) $data['moneyAmount']);
        self::assertSame($result['money']['currency'], (string) $data['moneyCurrency']);
        self::assertSame($result['dueDate'], ($data['dueDate'])->format('Y-m-d'));
        self::assertSame($result['qrOptions']['scale'], $data['qrCodeScale']);
        self::assertSame($result['qrOptions']['margin'], $data['qrCodeMargin']);
    }

    /**
     * Otestuje počáteční getters
     */
    public function testGetQrCodeSettingsWithoutSymbols(): void
    {
        $data = $this->getDataForObject();

        $this->object
            ->setIban($data['iban'])
            ->setMessage($data['message'])
            ->setDueDate($data['dueDate'])
            ->setMoneyAmount($data['moneyAmount'])
            ->setMoneyCurrency($data['moneyCurrency'])
            ->setQrCodeScale($data['qrCodeScale'])
            ->setQrCodeMargin($data['qrCodeMargin'])
        ;

        $result = $this->object->getQrCodeSettings();

        self::assertSame($result['iban'], $data['iban']);
        self::assertSame($result['money']['amount'], (string) $data['moneyAmount']);
        self::assertSame($result['money']['currency'], (string) $data['moneyCurrency']);
        self::assertSame($result['dueDate'], ($data['dueDate'])->format('Y-m-d'));
        self::assertSame($result['qrOptions']['scale'], $data['qrCodeScale']);
        self::assertSame($result['qrOptions']['margin'], $data['qrCodeMargin']);
        self::assertArrayNotHasKey('paymentIdentification', $result);
    }

    /**
     * @return array{
     *     iban: string, specificSymbol: int, constantSymbol: int, variableSymbol: int, message: string,
     *     dueDate: DateTime, moneyAmount: int, moneyCurrency: string, qrCodeScale: int, qrCodeMargin: int
     * }
     */
    private function getDataForObject(): array
    {
        return [
            'iban' => $this->faker->iban(),
            'specificSymbol' => $this->faker->randomNumber(),
            'constantSymbol' => $this->faker->randomNumber(),
            'variableSymbol' => $this->faker->randomNumber(),
            'message' => $this->faker->text(),
            'dueDate' => $this->faker->dateTime(),
            'moneyAmount' => $this->faker->randomNumber(),
            'moneyCurrency' => $this->faker->currencyCode,
            'qrCodeScale' => $this->faker->randomNumber(),
            'qrCodeMargin' => $this->faker->randomNumber(),
        ];
    }
}