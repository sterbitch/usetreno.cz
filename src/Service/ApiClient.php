<?php

namespace App\Service;

use DateTime;
use RuntimeException;
use JsonException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiClient
{
    private const API_URL = 'https://topapi.top-test.cz/';
    
    private HttpClientInterface $client;
    private ParameterBagInterface $parameterBag;

    private ?DateTime $tokenExpiration = null;
    private ?string $token = '';

    /**
     * @param HttpClientInterface   $client
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(HttpClientInterface $client, ParameterBagInterface $parameterBag)
    {
        $this->client = $client;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @throws RuntimeException
     * @throws JsonException
     */
    public function generateQrCode(array $data)
    {
        $currentTimestamp = (new DateTime('now'))->getTimezone();

        if (null === $this->tokenExpiration || $this->tokenExpiration->getTimestamp() < $currentTimestamp) {
            $this->getApiToken();
        }

        if ('' === trim($this->token)) {
            throw new RuntimeException('Token can not be empty');
        }

        $result = $this->makeRequest(
            'chameleon/api/v1/qr-code/create-for-bank-account-payment',
            [
                'Content-Type: application/json',
                'Authorization: Bearer ' . $this->token,
            ],
            json_encode($data, JSON_THROW_ON_ERROR),
        );

        if (isset($result['errors'])) {
            throw new RuntimeException($result['errors'][0]['message']);
        }

        if (!isset($result['data']['base64Data'])) {
            throw new RuntimeException('QR code can not be empty');
        }

        return $result['data']['base64Data'];
    }

    /**
     * @return void
     * @throws JsonException
     */
    private function getApiToken(): void
    {
        $userData = [
            'username' => $this->getUsername(),
            'password' => $this->getPassword(),
        ];

        $result = $this->makeRequest(
            'chameleon/api/v1/token',
            ['Content-Type: application/json'],
            json_encode($userData, JSON_THROW_ON_ERROR),
        );

        if (!isset($result['data']['token'])) {
            throw new RuntimeException('ApiToken can not be empty');
        }

        $this->token = $result['data']['token'];
        $this->tokenExpiration = new DateTime('now + 1 hour');
    }

    /**
     * @param string $url
     * @param string[] $headers
     * @param string $body
     *
     * @return array
     */
    private function makeRequest(string $url, array $headers, string $body): array
    {
        try {
            $response = $this->client->request('POST', self::API_URL . $url, [
                'headers' => $headers,
                'body' => $body,
            ]);

            return $response->toArray(false);
        } catch (
            TransportExceptionInterface|
            ClientExceptionInterface|
            RedirectionExceptionInterface|
            DecodingExceptionInterface|
            ServerExceptionInterface $e
        ) {
            throw new RuntimeException($e->getMessage());
        }
    }

    /**
     * @return string
     *
     * @throws RuntimeException
     */
    private function getUsername(): string
    {
        $username = $this->parameterBag->get('apiUser');

        if ('' === trim($username)) {
            throw new RuntimeException('Username can not be empty');
        }

        return $username;
    }

    /**
     * @return string
     *
     * @throws RuntimeException
     */
    private function getPassword(): string
    {
        $password = $this->parameterBag->get('apiPassword');

        if ('' === trim($password)) {
            throw new RuntimeException('Password can not be empty');
        }

        return $password;
    }
}
