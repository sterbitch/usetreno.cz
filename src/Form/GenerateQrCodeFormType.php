<?php

namespace App\Form;

use App\Form\Model\QrCode;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GenerateQrCodeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('iban', TextType::class, [
                'required' => true,
                'label' => 'IBAN',
            ])
            ->add('variableSymbol', IntegerType::class, [
                'required' => false,
                'label' => 'Variabilní symbol',
                'attr' => [
                    'min' => 1,
                ]
            ])
            ->add('specificSymbol', IntegerType::class, [
                'required' => false,
                'label' => 'Specifický symbol',
                'attr' => [
                    'min' => 1,
                ]
            ])
            ->add('constantSymbol', IntegerType::class, [
                'required' => false,
                'label' => 'Konstantní symbol',
                'attr' => [
                    'min' => 1,
                ]
            ])
            ->add('moneyAmount', IntegerType::class, [
                'required' => true,
                'label' => 'Částka',
                'attr' => [
                    'min' => 1,
                ]
            ])
            ->add('moneyCurrency', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    'Česká koruna' => 'CZK',
                ],
                'label' => 'Měna',
            ])
            ->add('dueDate', DateType::class, [
                'required' => true,
                'label' => 'Splatnost',
                'widget' => 'single_text',
                'data' => new DateTime('now'),
            ])
            ->add('message', TextareaType::class, [
                'required' => false,
                'label' => 'Zpráva',
            ])
            ->add('qrCodeScale', IntegerType::class, [
                'required' => true,
                'label' => 'Měřítko QR kódu',
                'empty_data' => 5,
            ])
            ->add('qrCodeMargin', IntegerType::class, [
                'required' => true,
                'label' => 'Odsazení QR kódu',
                'empty_data' => 20,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Vygenerovat QR kód',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => QrCode::class,
        ]);
    }
}
