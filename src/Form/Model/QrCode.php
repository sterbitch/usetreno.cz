<?php

namespace App\Form\Model;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class QrCode
{
    /**
     * @var string
     *
     * @Assert\NotBlank(message="iban can not be empty")
     */
    public string $iban;

    /**
     * @var int|null
     */
    public ?int $variableSymbol = null;

    /**
     * @var int|null
     */
    public ?int $specificSymbol = null;

    /**
     * @var int|null
     */
    public ?int $constantSymbol = null;

    /**
     * @var int
     *
     * @Assert\NotBlank(message="Money amount can not be null")
     */
    public int $moneyAmount;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="currency can not be empty")
     */
    public string $moneyCurrency;

    /**
     * @var DateTime
     *
     * @Assert\NotBlank(message="Due date can not be empty")
     */
    public DateTime $dueDate;

    /**
     * @var string|null
     */
    public ?string $message = null;

    /**
     * @var int|null
     */
    public ?int $qrCodeScale = 5;

    /**
     * @var int|null
     */
    public ?int $qrCodeMargin = 20;

    /**
     * @return string
     */
    public function getIban(): string
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     *
     * @return self
     */
    public function setIban(string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getVariableSymbol(): ?int
    {
        return $this->variableSymbol;
    }

    /**
     * @param int|null $variableSymbol
     *
     * @return self
     */
    public function setVariableSymbol(?int $variableSymbol): self
    {
        $this->variableSymbol = $variableSymbol;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSpecificSymbol(): ?int
    {
        return $this->specificSymbol;
    }

    /**
     * @param int|null $specificSymbol
     *
     * @return self
     */
    public function setSpecificSymbol(?int $specificSymbol): self
    {
        $this->specificSymbol = $specificSymbol;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getConstantSymbol(): ?int
    {
        return $this->constantSymbol;
    }

    /**
     * @param int|null $constantSymbol
     *
     * @return self
     */
    public function setConstantSymbol(?int $constantSymbol): self
    {
        $this->constantSymbol = $constantSymbol;

        return $this;
    }

    /**
     * @return int
     */
    public function getMoneyAmount(): int
    {
        return $this->moneyAmount;
    }

    /**
     * @param int $moneyAmount
     *
     * @return self
     */
    public function setMoneyAmount(int $moneyAmount): self
    {
        $this->moneyAmount = $moneyAmount;

        return $this;
    }

    /**
     * @return string
     */
    public function getMoneyCurrency(): string
    {
        return $this->moneyCurrency;
    }

    /**
     * @param string $moneyCurrency
     *
     * @return self
     */
    public function setMoneyCurrency(string $moneyCurrency): self
    {
        $this->moneyCurrency = $moneyCurrency;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDueDate(): DateTime
    {
        return $this->dueDate;
    }

    /**
     * @param DateTime $dueDate
     *
     * @return self
     */
    public function setDueDate(DateTime $dueDate): self
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     *
     * @return self
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQrCodeScale(): ?int
    {
        return $this->qrCodeScale;
    }

    /**
     * @param int|null $qrCodeScale
     *
     * @return self
     */
    public function setQrCodeScale(?int $qrCodeScale): self
    {
        $this->qrCodeScale = $qrCodeScale;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQrCodeMargin(): ?int
    {
        return $this->qrCodeMargin;
    }

    /**
     * @param int|null $qrCodeMargin
     *
     * @return self
     */
    public function setQrCodeMargin(?int $qrCodeMargin): self
    {
        $this->qrCodeMargin = $qrCodeMargin;

        return $this;
    }

    /**
     * @return array<string, string|array<string, string|int>>
     */
    public function getQrCodeSettings(): array
    {
        $data = [
            'iban' => $this->getIban(),
            'money' => [
                'amount' => (string) $this->getMoneyAmount(),
                'currency' => $this->getMoneyCurrency(),
            ],
            'dueDate' => $this->getDueDate()->format('Y-m-d'),
            'qrOptions' => [
                'scale' => $this->getQrCodeScale(),
                'margin' => $this->getQrCodeMargin(),
            ],
        ];

        if (null !== $this->getVariableSymbol()) {
            $data['paymentIdentification']['variableSymbol'] = (string) $this->getVariableSymbol();
        }

        if (null !== $this->getSpecificSymbol()) {
            $data['paymentIdentification']['specificSymbol'] = (string) $this->getSpecificSymbol();
        }

        if (null !== $this->getConstantSymbol()) {
            $data['paymentIdentification']['constantSymbol'] = (string) $this->getConstantSymbol();
        }

        if (null !== $this->getMessage()) {
            $data['message'] = $this->getMessage();
        }

        return $data;
    }
}
