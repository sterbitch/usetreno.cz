<?php

namespace App\Controller;

use App\Form\GenerateQrCodeFormType;
use App\Form\Model\QrCode;
use App\Service\ApiClient;
use JsonException;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QrGeneratorController extends AbstractController
{
    private ApiClient $client;

    /**
     * @param ApiClient $client
     */
    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/", name="app_index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $qrCode = new QrCode();
        $qrCodeImage = '';

        $form = $this->createForm(GenerateQrCodeFormType::class, $qrCode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $qrCodeImage = $this->client->generateQrCode($qrCode->getQrCodeSettings());
            } catch (JsonException|RuntimeException $e) {
                $this->addFlash('danger', $e->getMessage());

                return $this->redirectToRoute('app_index');
            }

            $this->addFlash('success', 'QR kód byl vygenerován');
        }

        return $this->render('index.html.twig', [
            'form' => $form->createView(),
            'qrCode' => $qrCodeImage,
        ]);
    }
}