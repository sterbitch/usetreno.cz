# Generátor QR kódů #

### Instalace ###

>composer install

### Doplňující informace ###
Vytvořte si soubory 

<code>.env.local</code>

<code>.env.test.local</code>

vložte do souborů přihlašovací jméno a heslo pro API

<code>API_USER=developer</code>

<code>API_PASSWORD=developer123</code>